const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const taskListRoutes = require('./routes/taskListRoutes');
const taskRoutes = require('./routes/taskRoutes');

const app = express();
app.use(express.json());
app.use(cors());


app.use('/api/users', userRoutes);
app.use('/api/taskLists', taskListRoutes);
app.use('/api/tasks', taskRoutes);

const PORT = process.env.PORT || 3000;
const DATABASE_URL = "mongodb+srv://Hunter:Hunting@cluster0.7zjgbh9.mongodb.net/test?retryWrites=true&w=majority";
mongoose.connect(DATABASE_URL, { 
    useNewUrlParser: true,
    useUnifiedTopology: true
})
  .then(() => console.log("MongoDB connected"))
  .catch(err => console.log(err));

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});