const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
  taskListId: { type: mongoose.Schema.Types.ObjectId, ref: 'TaskList' },
  description: { type: String, required: true },
  details: String,
  dueDate: { type: Date },
  completed: { type: Boolean, default: false }
});

module.exports = mongoose.model('Task', taskSchema);