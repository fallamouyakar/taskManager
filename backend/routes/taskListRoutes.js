const express = require('express');
const router = express.Router();
const taskListController = require('../controllers/taskListController');
const auth = require('../middleware/auth');

// Create a new task list
router.post('/', auth, taskListController.createTaskList);

// Get all task lists for a user
router.get('/', auth, taskListController.getTaskLists);

// Update a specific task list
router.put('/:id', auth, taskListController.updateTaskList);

// Delete a specific task list
router.delete('/:id', auth, taskListController.deleteTaskList);

module.exports = router;
