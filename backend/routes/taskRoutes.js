const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskController');
const auth = require('../middleware/auth');


// Create a new task in a task list
router.post('/', auth, taskController.createTask);

// Get all tasks by taskListId
router.get('/', auth, taskController.getTasks);

// Update a specific task
router.put('/:id', auth, taskController.updateTask);

// Delete a specific task
router.delete('/:id', auth, taskController.deleteTask);

module.exports = router;
