const TaskList = require('../models/TaskList');
const Task = require('../models/Task');

// Create TaskList
exports.createTaskList = async (req, res) => {
  try {
    const { name, userId } = req.body;
    const newTaskList = new TaskList({ name, userId });
    await newTaskList.save();
    res.status(201).json(newTaskList);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Read TaskLists
exports.getTaskLists = async (req, res) => {
  try {
    const taskLists = await TaskList.find({ userId: req.user._id });
    res.status(200).json(taskLists);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Update TaskList
exports.updateTaskList = async (req, res) => {
  try {
    const { id } = req.params;
    const updates = req.body;
    const taskList = await TaskList.findByIdAndUpdate(id, updates, { new: true });
    res.status(200).json(taskList);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Delete TaskList
exports.deleteTaskList = async (req, res) => {
  try {
    const { id } = req.params;
    await Task.deleteMany({ taskListId: id });
    await TaskList.findByIdAndDelete(id);
    res.status(200).json({ message: "Task list and all associated tasks deleted successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
