export class User {
    _id?: string;
    username: string;
    email: string;
    password?: string;  // Optional because we don't receive the password from backend on fetch
  
    constructor(username: string, email: string, password?: string, _id?: string) {
      this.username = username;
      this.email = email;
      this.password = password;
      this._id = _id;
    }
  }
  