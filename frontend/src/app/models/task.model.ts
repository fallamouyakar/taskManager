export class Task {
    _id?: string;
    taskListId: string;
    description: string;
    details?: string;
    dueDate?: Date;
    completed: boolean;
  
    constructor(
      taskListId: string,
      description: string,
      completed: boolean,
      details?: string,
      dueDate?: Date,
      _id?: string
    ) {
      this.taskListId = taskListId;
      this.description = description;
      this.details = details;
      this.dueDate = dueDate;
      this.completed = completed;
      this._id = _id;
    }
  }
  