export class TaskList {
    _id?: string;
    name: string;
    userId: string;
    tasks?: string[];  // This could be an array of task IDs or Task objects
  
    constructor(name: string, userId: string, _id?: string, tasks?: string[]) {
      this.name = name;
      this.userId = userId;
      this._id = _id;
      this.tasks = tasks;
    }
  }
  