import { TestBed } from '@angular/core/testing';

import { TaskListclearService } from './task-listclear.service';

describe('TaskListclearService', () => {
  let service: TaskListclearService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TaskListclearService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
