import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TaskList } from '../models/task-list.model';

@Injectable({
  providedIn: 'root'
})
export class TaskListService {

  private apiUrl = `http://localhost:3000/api/taskLists`;

  constructor(private http: HttpClient) { }

  getTaskLists(): Observable<TaskList[]> {
    return this.http.get<TaskList[]>(this.apiUrl);
  }

  createTaskList(taskList: TaskList): Observable<TaskList> {
    return this.http.post<TaskList>(this.apiUrl, taskList);
  }

  deleteTaskList(id: string): Observable<any> {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }
}
