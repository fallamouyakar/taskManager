import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Task } from '../models/task.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private apiUrl = `http://localhost:3000/api/tasks`;

  constructor(private http: HttpClient) { }

  getTasks(taskListId: string): Observable<Task[]> {
    return this.http.get<Task[]>(`${this.apiUrl}?taskListId=${taskListId}`);
  }

  createTask(task: Task): Observable<Task> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post<Task>(this.apiUrl, task, { headers });
  }

  updateTask(task: Task): Observable<Task> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.put<Task>(`${this.apiUrl}/${task._id}`, task, { headers });
  }

  deleteTask(id: string): Observable<any> {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }

  // Toggle the completion status of a task
  toggleTaskCompletion(task: Task): Observable<Task> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    task.completed = !task.completed;  // Toggle completion status
    return this.http.put<Task>(`${this.apiUrl}/${task._id}`, task, { headers });
  }
}
